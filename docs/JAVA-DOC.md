# JAVADOC

## Overview
The Maven Javadoc Plugin uses the Javadoc tool to generate javadocs for the specified project.

## Generate docs
To generate javadocs for the project, run:

```bash
mvn javadoc:javadoc
```

For aggregation, run:

```bash
mvn javadoc:aggregate
```

For other goals, see the [documentation](http://maven.apache.org/plugins/maven-javadoc-plugin/index.html).